﻿using Ejemplo_01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo_01.Pages
{
    public partial class ListaCantones : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using (BasesDatosEntities db = new BasesDatosEntities())
                {
                    var listaCantones = (from c in db.spConsultarCantones()
                                         select c).ToList();
                    grdListaCantones.DataSource = listaCantones;
                    grdListaCantones.DataBind();
                }
            }
            catch
            {

            }
        }

        protected void grdListaCantones_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}