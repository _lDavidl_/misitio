﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrearProvincia.aspx.cs" Inherits="Ejemplo_01.Pages.CrearProvincia" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<h1>Crear Provincia</h1>
<div>
    <span>Provincia</span><br />
    <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
    <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox><br />
    <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
    <a href="ListaProvincias.aspx" class="btn btn-default">Cancelar</a>
</div>

</asp:Content>
