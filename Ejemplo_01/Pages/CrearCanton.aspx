﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrearCanton.aspx.cs" Inherits="Ejemplo_01.Pages.CrearCanton" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Crear cantón</h1>
    <div>
        <asp:Label ID="lblMensaje" runat="server" Text=""></asp:Label>
    </div>
    <div>
        <span>Provincia</span>
        <asp:DropDownList ID="ddnProvincia" AutoPostBack="true" OnSelectedIndexChanged="ddnProvincia_SelectedIndexChanged" runat="server"></asp:DropDownList> <br /> <br />

        <span>Cantón</span>
        <asp:TextBox ID="txtNombre" runat="server"></asp:TextBox> <br /> <br />
        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClick="btnGuardar_Click" />
        <a href="ListaCantones.aspx">Cancelar</a>
    </div>

</asp:Content>
