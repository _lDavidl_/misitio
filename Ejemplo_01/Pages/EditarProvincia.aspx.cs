﻿using Ejemplo_01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo_01.Pages
{
    public partial class EditarProvincia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (this.IsPostBack == false)
                {
                    int idProvincia = int.Parse(Request.QueryString["id"]);
                    using (BasesDatosEntities db = new BasesDatosEntities())
                    {
                        var provincia = (from p in db.spConsultarProvinciaPorId(idProvincia)
                                         select p).FirstOrDefault();

                        hdnIdProvincia.Value = provincia.idProvincia.ToString();
                        txtNombre.Text = provincia.nombre;
                    }
                }
            }
            catch
            {

            }
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if(txtNombre.Text != "")
                {
                    using(BasesDatosEntities db = new BasesDatosEntities())
                    {
                        int idProvincia = int.Parse(hdnIdProvincia.Value);
                        string nombre = txtNombre.Text;
                        string estado = "A";

                        db.spEditarProvincia(idProvincia, nombre, estado);
                        db.SaveChanges();

                    }
                    lblMensaje.Text = "Se ha editado la infomacion de la provincia";
                }
                else
                {
                    lblMensaje.Text = "Todos los campos son requeridos";
                }
            }
            catch
            {
                lblMensaje.Text = "Ha ocurrido un error";
            }
          
        }

        protected void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                using (BasesDatosEntities db = new BasesDatosEntities())
                {
                    int idProvincia = int.Parse(hdnIdProvincia.Value);

                    db.spEliminarProvincia(idProvincia);
                    db.SaveChanges();
                }
                Response.Redirect("~/Pages/ListaProvincias.aspx");
            }
            catch
            {
                lblMensaje.Text = "Ha ocurrido un error";
            }
        }
    }
}