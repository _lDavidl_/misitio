﻿using Ejemplo_01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo_01.Pages
{
    public partial class ListaProvincias : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                using(BasesDatosEntities bd = new BasesDatosEntities())
                {
                    var listaProvincias = (from p in bd.spConsultarProvincias()
                                           select p).ToList();

                    grdProvincias.DataSource = listaProvincias;
                    grdProvincias.DataBind();
                }
            }
            catch
            {

            }
        }
    }
}