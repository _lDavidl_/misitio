﻿using Ejemplo_01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo_01.Pages
{
    public partial class CrearCanton : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack == false)
            {
                using(BasesDatosEntities db = new BasesDatosEntities())
                {
                    var listaProvincias = (from p in db.spConsultarProvincias()
                                           select p).ToList();
                    ddnProvincia.Items.Add(new ListItem("Seleccione una provincia", ""));
                    foreach(var provincia in listaProvincias)
                    {
                        var item = new ListItem(provincia.provincia, provincia.idProvincia.ToString());
                        ddnProvincia.Items.Add(item);
                    }
                    ddnProvincia.DataBind();
                }
            }
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                string idProvincia = ddnProvincia.SelectedItem.Value;
                string nombre = txtNombre.Text;
                
                if(idProvincia!= "" && nombre != "")
                {
                    DateTime fechaCreacion = DateTime.Now;

                    using (BasesDatosEntities db = new BasesDatosEntities())
                    {
                        db.spCrearCanton(int.Parse(idProvincia), nombre, fechaCreacion);
                        db.SaveChanges();
                    }
                    lblMensaje.Text = "Se ha creado un nuevo canton en la base de datos";
                    txtNombre.Text = "";
                }
                else
                {
                    lblMensaje.Text = "Todos los campos son requeridos";
                }
            }
            catch
            {
                lblMensaje.Text = "Ha ocurrido un error";
            }
        }

        protected void ddnProvincia_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}