﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaCantones.aspx.cs" Inherits="Ejemplo_01.Pages.ListaCantones" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <h1>Lista de cantones</h1>
    <div>
        <a href="CrearCanton.aspx">Crear Cantón</a>
    </div>
    <div>
        <asp:GridView ID="grdListaCantones" runat="server" AutoGenerateColumns="false" OnSelectedIndexChanged="grdListaCantones_SelectedIndexChanged" CssClass="table">
            <Columns>
                <asp:BoundField DataField="idCanton" HeaderText="ID" />
                <asp:BoundField DataField="provincia" HeaderText="Provincia" />
                <asp:BoundField DataField="canton" HeaderText="Cantón" />
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
