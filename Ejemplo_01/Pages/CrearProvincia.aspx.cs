﻿using Ejemplo_01.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Ejemplo_01.Pages
{
    public partial class CrearProvincia : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtNombre.Text != "")
                {
                    DateTime fechaCreacion = DateTime.Now;

                    using (BasesDatosEntities db = new BasesDatosEntities())
                    {
                        db.spCrearProvincia(txtNombre.Text, fechaCreacion);
                        db.SaveChanges();

                    }
                    lblMensaje.Text = "Se ha creado una nueva provincia";
                    txtNombre.Text = "";
                }
                else
                {
                    lblMensaje.Text = "Todos los campos del formulario deben ser requeridos";

                }
            }
            catch
            {
                lblMensaje.Text = "Ha ocurrido un problema";
            }
        }
    }
}