﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListaProvincias.aspx.cs" Inherits="Ejemplo_01.Pages.ListaProvincias" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <asp:GridView ID="grdProvincias" runat="server" AutoGenerateColumns="false">
                <Columns>
                    <asp:BoundField DataField="idProvincia" HeaderText="Id Provincia" />
                    <asp:BoundField DataField="provincia" HeaderText="Nombre de provincia" />
                    <asp:TemplateField>
                        <ItemTemplate>
                            <a href="EditarProvincia.aspx?id=<%#Eval("idProvincia")%>">Editar</a>
                        </ItemTemplate>

                    </asp:TemplateField>
                    
                </Columns>

            </asp:GridView>
        </div>
    </form>
</body>
</html>
